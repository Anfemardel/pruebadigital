package com.example.pruebaDigital.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="productos")
public class Producto implements Serializable {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idProducto;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="precio")
	private int precio;
	
	@Column(name="inventario")
	private int inventario;
	
	@OneToMany(mappedBy="productos")
	@JsonIgnore
	private Set<Pedido> prodidos;
	
	public Producto(String nombre, int precio, int inventario, Set<Pedido> prodidos) {
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.inventario = inventario;
		this.prodidos = prodidos;
	}
	
	
	
	
	
	public Set<Pedido> getProdidos() {
		return prodidos;
	}





	public void setProdidos(Set<Pedido> prodidos) {
		this.prodidos = prodidos;
	}





	public Producto(String nombre, int precio, int inventario) {
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.inventario = inventario;
	}
	public Producto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public int getInventario() {
		return inventario;
	}
	public void setInventario(int inventario) {
		this.inventario = inventario;
	}
	
	
}
