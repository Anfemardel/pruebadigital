package com.example.pruebaDigital.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="usuario")
public class Usuario implements Serializable {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUsuario;
	
	@Column(name="usuario")
	private String usuario;
	
	@Column(name="contraseña")
	private String contraseña;
	
	@Column(name="rol")
	private int rol;
	
	@OneToMany(mappedBy="usuarios")
	private Set<Pedido> prodidos;
	
	

	public Usuario(String usuario, String contraseña, int rol, Set<Pedido> prodidos) {
		super();
		this.usuario = usuario;
		this.contraseña = contraseña;
		this.rol = rol;
		this.prodidos = prodidos;
	}

	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public int getRol() {
		return rol;
	}

	public void setRol(int rol) {
		this.rol = rol;
	}

	public Set<Pedido> getProdidos() {
		return prodidos;
	}

	public void setProdidos(Set<Pedido> prodidos) {
		this.prodidos = prodidos;
	}
	
	

}
