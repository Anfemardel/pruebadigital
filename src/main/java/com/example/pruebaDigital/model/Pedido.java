package com.example.pruebaDigital.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="pedidos")
public class Pedido implements Serializable{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPedidos;
	
	@Column(name="fecha")
	private String fecha;
	
	@Column(name="autorizar")
	private int autorizar;
	
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	@JoinColumn(name="id_producto")
	@JsonIgnore
	private Producto productos;
	
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	@JoinColumn(name="id_usuario")
	@JsonIgnore
	private Usuario usuarios;
	
	

	public Pedido() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Pedido(String fecha, int autorizar, Producto productos, Usuario usuarios) {
		super();
		this.fecha = fecha;
		this.autorizar = autorizar;
		/*this.productos = productos;
		this.usuarios = usuarios;*/
	}



	public int getIdPedidos() {
		return idPedidos;
	}

	public void setIdPedidos(int idPedidos) {
		this.idPedidos = idPedidos;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getAutorizar() {
		return autorizar;
	}

	public void setAutorizar(int autorizar) {
		this.autorizar = autorizar;
	}

	/*public Producto getProductos() {
		return productos;
	}*/

	/*public void setProductos(Producto productos) {
		this.productos = productos;
	}*/

	/*public Usuario getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuario usuarios) {
		this.usuarios = usuarios;
	}*/
	
	
	
		
}
