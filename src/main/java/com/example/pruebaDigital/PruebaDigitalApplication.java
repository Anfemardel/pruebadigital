package com.example.pruebaDigital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication(exclude=HibernateJpaAutoConfiguration.class)
public class PruebaDigitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaDigitalApplication.class, args);
	}

}
