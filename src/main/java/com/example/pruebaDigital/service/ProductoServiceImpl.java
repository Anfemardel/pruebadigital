package com.example.pruebaDigital.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.pruebaDigital.dao.ProductoDao;
import com.example.pruebaDigital.model.Producto;

@Service("productoService")
@Transactional
public class ProductoServiceImpl implements ProductoService{

	
	@Autowired
	private ProductoDao _productoDao;
	@Override
	public void saveProducto(Producto producto) {
		// TODO Auto-generated method stub
		_productoDao.saveProducto(producto);
	}

	@Override
	public void deleteProductoById(int idProducto) {
		// TODO Auto-generated method stub
		_productoDao.deleteProductoById(idProducto);
	}

	@Override
	public void updateProducto(Producto producto) {
		// TODO Auto-generated method stub
		_productoDao.updateProducto(producto);
	}

	@Override
	public List<Producto> fintAllProductos() {
		// TODO Auto-generated method stub
		return _productoDao.fintAllProductos();
	}

	@Override
	public Producto fintById(int idProducto) {
		// TODO Auto-generated method stub
		return _productoDao.fintById(idProducto);
	}

	@Override
	public Producto fintByName(String nombre) {
		// TODO Auto-generated method stub
		return _productoDao.fintByName(nombre);
	}

}
