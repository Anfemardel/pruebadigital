package com.example.pruebaDigital.service;

import java.util.List;

import com.example.pruebaDigital.model.Usuario;

public interface UsuarioService {

	void saveUsuario(Usuario usuario);
	
	void deleteUsuarioById(int idUsuario);
	
	void updateUsuario(Usuario usuario);
	
	List<Usuario> fintAllUsuarios();
	
	Usuario fintById(int idUsuario);
}
