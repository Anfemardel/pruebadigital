package com.example.pruebaDigital.service;

import java.util.List;

import com.example.pruebaDigital.model.Pedido;

public interface PedidoService {

	void savePedido(Pedido pedido);
	
	void deletePedidoById(int idPedido);
	
	void updatePedido(Pedido pedido);
	
	List<Pedido> fintAllPedidos();
	
	Pedido fintById(int idPedido);
}
