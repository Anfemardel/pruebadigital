package com.example.pruebaDigital.service;

import java.util.List;

import com.example.pruebaDigital.model.Producto;

public interface ProductoService {
	
	void saveProducto(Producto producto);
	
	void deleteProductoById(int idProducto);
	
	void updateProducto(Producto producto);
	
	List<Producto> fintAllProductos();
	
	Producto fintById(int idProducto);
	
	Producto fintByName(String nombre);

}
