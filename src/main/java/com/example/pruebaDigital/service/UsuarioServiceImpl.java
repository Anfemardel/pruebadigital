package com.example.pruebaDigital.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.pruebaDigital.dao.UsuarioDao;
import com.example.pruebaDigital.model.Usuario;

@Service("usuarioService")
@Transactional
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioDao _usuarioDao;
	
	@Override
	public void saveUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		_usuarioDao.saveUsuario(usuario);
	}

	@Override
	public void deleteUsuarioById(int idUsuario) {
		// TODO Auto-generated method stub
		_usuarioDao.deleteUsuarioById(idUsuario);
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		_usuarioDao.updateUsuario(usuario);
	}

	@Override
	public List<Usuario> fintAllUsuarios() {
		// TODO Auto-generated method stub
		return _usuarioDao.fintAllUsuarios();
	}

	@Override
	public Usuario fintById(int idUsuario) {
		// TODO Auto-generated method stub
		return _usuarioDao.fintById(idUsuario);
	}

}
