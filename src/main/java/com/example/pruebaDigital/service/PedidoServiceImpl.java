package com.example.pruebaDigital.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.pruebaDigital.dao.PedidoDao;
import com.example.pruebaDigital.model.Pedido;

@Service("pedidoService")
@Transactional
public class PedidoServiceImpl implements PedidoService {

	
	@Autowired
	private PedidoDao _pedidoDao;
	
	@Override
	public void savePedido(Pedido pedido) {
		// TODO Auto-generated method stub
		_pedidoDao.savePedido(pedido);
	}

	@Override
	public void deletePedidoById(int idPedido) {
		// TODO Auto-generated method stub
		_pedidoDao.deletePedidoById(idPedido);
	}

	@Override
	public void updatePedido(Pedido pedido) {
		// TODO Auto-generated method stub
		_pedidoDao.updatePedido(pedido);
	}

	@Override
	public List<Pedido> fintAllPedidos() {
		// TODO Auto-generated method stub
		return _pedidoDao.fintAllPedidos();
	}

	@Override
	public Pedido fintById(int idPedido) {
		// TODO Auto-generated method stub
		return _pedidoDao.fintById(idPedido);
	}

}
