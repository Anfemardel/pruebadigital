package com.example.pruebaDigital.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.example.pruebaDigital.model.Pedido;

@Repository
@Transactional
public class PedidoDaoImpl extends AbstractSession implements PedidoDao {

	@Override
	public void savePedido(Pedido pedido) {
		// TODO Auto-generated method stub
		getSession().persist(pedido);
		
	}

	@Override
	public void deletePedidoById(int idPedido) {
		// TODO Auto-generated method stub
		Pedido pedido = fintById(idPedido);
		if(pedido != null) {
			getSession().delete(pedido);
		}
	}

	@Override
	public void updatePedido(Pedido pedido) {
		// TODO Auto-generated method stub
		getSession().update(pedido);
	}

	@Override
	public List<Pedido> fintAllPedidos() {
		// TODO Auto-generated method stub
		return getSession().createQuery("from Pedido").list();
	}

	@Override
	public Pedido fintById(int idPedido) {
		// TODO Auto-generated method stub
		return (Pedido)getSession().get(Pedido.class, idPedido);
	}


}
