package com.example.pruebaDigital.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.example.pruebaDigital.model.Usuario;

@Repository
@Transactional
public class UsuarioDaoImpl extends AbstractSession implements UsuarioDao{

	@Override
	public void saveUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		getSession().persist(usuario);
		
	}

	@Override
	public void deleteUsuarioById(int idUsuario) {
		// TODO Auto-generated method stub
		Usuario usuario = fintById(idUsuario);
		if(usuario != null) {
			getSession().delete(usuario);
		}
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		getSession().update(usuario);
	}

	@Override
	public List<Usuario> fintAllUsuarios() {
		// TODO Auto-generated method stub
		return getSession().createQuery("from Usuario").list();
	}

	@Override
	public Usuario fintById(int idUsuario) {
		// TODO Auto-generated method stub
		return (Usuario)getSession().get(Usuario.class, idUsuario);
	}

}
