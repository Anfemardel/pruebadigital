package com.example.pruebaDigital.dao;

import java.util.List;

import com.example.pruebaDigital.model.Producto;

public interface ProductoDao {

	void saveProducto(Producto producto);
	
	void deleteProductoById(int idProducto);
	
	void updateProducto(Producto producto);
	
	List<Producto> fintAllProductos();
	
	Producto fintById(int idProducto);
	
	Producto fintByName(String nombre);
	
}
