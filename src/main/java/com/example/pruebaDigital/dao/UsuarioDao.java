package com.example.pruebaDigital.dao;

import java.util.List;

import com.example.pruebaDigital.model.Usuario;

public interface UsuarioDao {

	void saveUsuario(Usuario usuario);
	
	void deleteUsuarioById(int idUsuario);
	
	void updateUsuario(Usuario usuario);
	
	List<Usuario> fintAllUsuarios();
	
	Usuario fintById(int idUsuario);
}
