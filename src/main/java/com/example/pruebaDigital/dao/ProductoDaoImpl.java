package com.example.pruebaDigital.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.example.pruebaDigital.model.Producto;

@Repository
@Transactional
public class ProductoDaoImpl extends AbstractSession implements ProductoDao {

	@Override
	public void saveProducto(Producto producto) {
		// TODO Auto-generated method stub
		getSession().persist(producto);
	}

	@Override
	public void deleteProductoById(int idProducto) {
		// TODO Auto-generated method stub
		Producto producto = fintById(idProducto);
		if(producto != null) {
			getSession().delete(producto);
		}
	}

	@Override
	public void updateProducto(Producto producto) {
		// TODO Auto-generated method stub
		getSession().update(producto);
	}

	@Override
	public List<Producto> fintAllProductos() {
		// TODO Auto-generated method stub
		return getSession().createQuery("from Producto").list();
	}

	@Override
	public Producto fintById(int idProducto) {
		// TODO Auto-generated method stub
		return (Producto)getSession().get(Producto.class, idProducto);
	}

	@Override
	public Producto fintByName(String nombre) {
		// TODO Auto-generated method stub
		return (Producto) getSession().createQuery(
				"from Producto WHERE nombre = :nombre").setParameter("nombre", nombre).uniqueResult();		
		
	}

}
