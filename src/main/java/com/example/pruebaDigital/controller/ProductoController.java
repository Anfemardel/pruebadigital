package com.example.pruebaDigital.controller;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.pruebaDigital.model.Producto;
import com.example.pruebaDigital.service.ProductoService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/v1")
public class ProductoController {
	
	@Autowired
	ProductoService _productoService;
	
	//GET
	@RequestMapping(value="/producto", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Producto>> getProducto(){
		
		List<Producto> producto = new ArrayList<>();
		producto = _productoService.fintAllProductos();
		
		if(producto.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Producto>>(producto, HttpStatus.OK);
	}
	
	//GET
	@RequestMapping(value="/producto/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Producto> getProductoById(@PathVariable("id") int idProducto){
		
		if (idProducto <=0) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		Producto producto = _productoService.fintById(idProducto);
		if(producto == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}
	
	//POST
	@RequestMapping(value="/producto", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createProducto(@RequestBody Producto producto, UriComponentsBuilder uriComponentBuilder){
		if(producto.getNombre().equals(null) || producto.getNombre().isEmpty() ) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		if(_productoService.fintByName(producto.getNombre()) != null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		_productoService.saveProducto(producto);
		Producto producto2 = _productoService.fintByName(producto.getNombre());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(
				uriComponentBuilder.path("/v1/producto/{id}")
				.buildAndExpand(producto2.getIdProducto())
				.toUri()
				);
		
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	//UPDATE
	@RequestMapping(value="/producto/{id}", method = RequestMethod.PATCH, headers = "Accept=application/json")
	public ResponseEntity<?> updateProducto(@PathVariable("id") int idProducto, @RequestBody Producto producto){
		Producto currentProducto = _productoService.fintById(idProducto);
		if (idProducto <=0) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		if(currentProducto==null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			
		}
		currentProducto.setNombre(producto.getNombre());
		currentProducto.setPrecio(producto.getPrecio());
		currentProducto.setInventario(producto.getInventario());
		
		_productoService.updateProducto(currentProducto);
		return new ResponseEntity<Producto>(currentProducto,HttpStatus.OK);
	}
	
	//DELETE
	@RequestMapping(value="/producto/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deleteProducto(@PathVariable("id") int idProducto){
		if (idProducto <=0) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		Producto producto = _productoService.fintById(idProducto);
		if(producto==null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			
		}
		 _productoService.deleteProductoById(idProducto);
		 return new ResponseEntity<Producto>(HttpStatus.OK);
	}
}
