package com.example.pruebaDigital.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.pruebaDigital.model.Pedido;
import com.example.pruebaDigital.service.PedidoService;

@Controller
@RequestMapping("/v1")
public class PedidoController {
	
	@Autowired
	PedidoService _pedidoService;

	
		//GET
	
		@RequestMapping(value="/pedido", method = RequestMethod.GET, headers = "Accept=application/json")
		public ResponseEntity<List<Pedido>> getPedido(){
			
			List<Pedido> pedido = new ArrayList<>();
			
			pedido = _pedidoService.fintAllPedidos();
			
			if(pedido.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<Pedido>>(pedido, HttpStatus.OK);
		}
		
		/*//GET
		@RequestMapping(value="/producto/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
		public ResponseEntity<Producto> getProductoById(@PathVariable("id") int idProducto){
			
			if (idProducto <=0) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			
			Producto producto = _productoService.fintById(idProducto);
			if(producto == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<Producto>(producto, HttpStatus.OK);
		}*/
		
		//POST
		@RequestMapping(value="/pedido", method = RequestMethod.POST, headers = "Accept=application/json")
		public ResponseEntity<?> createPedido(@RequestBody Pedido pedido, UriComponentsBuilder uriComponentBuilder){
			/*if(pedido.getNombre().equals(null) || pedido.getNombre().isEmpty() ) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			if(_pedidoService.fintByName(pedido.getNombre()) != null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}*/
			
			_pedidoService.savePedido(pedido);
			
			return new ResponseEntity<String>(HttpStatus.CREATED);
		}
		/*
		//UPDATE
		@RequestMapping(value="/producto/{id}", method = RequestMethod.PATCH, headers = "Accept=application/json")
		public ResponseEntity<?> updateProducto(@PathVariable("id") int idProducto, @RequestBody Producto producto){
			Producto currentProducto = _productoService.fintById(idProducto);
			if (idProducto <=0) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			if(currentProducto==null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
				
			}
			currentProducto.setNombre(producto.getNombre());
			currentProducto.setPrecio(producto.getPrecio());
			currentProducto.setInventario(producto.getInventario());
			
			_productoService.updateProducto(currentProducto);
			return new ResponseEntity<Producto>(currentProducto,HttpStatus.OK);
		}
		
		//DELETE
		@RequestMapping(value="/producto/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
		public ResponseEntity<?> deleteProducto(@PathVariable("id") int idProducto){
			if (idProducto <=0) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			
			Producto producto = _productoService.fintById(idProducto);
			if(producto==null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
				
			}
			 _productoService.deleteProductoById(idProducto);
			 return new ResponseEntity<Producto>(HttpStatus.OK);
		}*/
}
